#![deny(clippy::all)]
#![allow(clippy::unnecessary_wraps)] // don't care: winding down this project

use clap::{App, Arg, SubCommand};

mod lib {
    pub mod cache;
    pub mod ghrtask;
    pub mod task;
    pub mod version;
}
mod tasks;
mod utils {
    pub mod env;
    pub mod fs;
    pub mod github;
    pub mod golang;
    pub mod http;
    pub mod process;
}

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .subcommand(
            SubCommand::with_name("all")
                .about("sync / update my settings and packages on this computer"),
        )
        .subcommand(
            SubCommand::with_name("some")
                .arg(Arg::with_name("tasks"))
                .about("run specific comma-separated tasks"),
        )
        .subcommand(SubCommand::with_name("env").about("export generated environment variables"))
        .get_matches();

    if let Some(_matches) = matches.subcommand_matches("all") {
        tasks::all();
        return;
    }

    if let Some(matches) = matches.subcommand_matches("some") {
        if matches.is_present("tasks") {
            tasks::some(matches.value_of("tasks").unwrap());
        } else {
            println!("Error: comma-separated list of tasks is mandatory for 'some'");
        }
    }
}
