use std::{env::consts::OS, str};

pub fn os() -> &'static str {
    if OS == "macos" {
        "darwin"
    } else {
        OS
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn os_is_not_macos() {
        assert_ne!(os(), "macos");
    }
}
