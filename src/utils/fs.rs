use std::{self, path::Path};
#[cfg(unix)]
use std::{fs::File, os::unix::fs::PermissionsExt};

#[cfg(unix)]
pub fn set_executable<P>(target: P) -> std::io::Result<()>
where
    P: AsRef<Path>,
{
    let file = File::open(target.as_ref()).unwrap();
    let mut perms = file.metadata().unwrap().permissions();
    perms.set_mode(0o755); // a+rx, u+w
    file.set_permissions(perms)
}

#[cfg(not(unix))]
pub fn set_executable<P>(_target: P) -> std::io::Result<()>
where
    P: AsRef<Path>,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    #[test]
    fn check_cargo() {
        let file_path = Path::new(env!("CARGO"));
        assert!(!file_path.is_dir());
    }

    #[test]
    fn check_cargo_manifest_dir() {
        let project_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
        assert!(project_dir.is_dir());
    }
}
