use std::collections::HashMap;

use crate::lib::task::Task;

mod hadolint;
mod vscode;

pub fn all() {
    // resource utilisation goals:
    // - distinguish CPU-heavy from I/O-heavy tasks
    // - run a CPU-heavy task concurrently with an I/O-heavy task
    // - serialise tasks of the same type to avoid clogging pipes
    // TODO: realise these goals :)
    // TODO: maybe queue I/O within GitHub and HTTP helpers

    let tasks = mapping();
    for s in sequence() {
        if let Some(t) = tasks.get(&s) {
            t.sync_then_update();
        }
    }
}

pub fn some<S>(input: S)
where
    S: AsRef<str>,
{
    let tasks = mapping();
    for name in input.as_ref().split(',') {
        if let Some(task) = tasks.get(name) {
            task.sync_then_update();
        }
    }
}

fn sequence() -> Vec<String> {
    vec![hadolint::task().name, vscode::task().name]
}

fn mapping() -> HashMap<String, Task> {
    let mut map = HashMap::<String, Task>::new();
    map.insert(String::from("hadolint"), hadolint::task());
    map.insert(String::from("vscode"), vscode::task());
    map
}

#[cfg(test)]
mod tests {
    use std::{fs, path::PathBuf};

    use super::*;

    #[test]
    fn sequence_lists_all_tasks() {
        let seq = sequence();

        let entries = fs::read_dir(PathBuf::from("src/tasks")).expect("must read");
        let mut count = 0;
        for entry in entries.flatten() {
            let name = entry
                .file_name()
                .to_string_lossy()
                .into_owned()
                .replace(".rs", "");
            if name == "local" || name == "mod" {
                continue;
            }
            count += 1;
            if !seq.contains(&name) {
                assert_eq!("", name);
            }

            assert!(seq.contains(&name));
        }
        assert_eq!(count, seq.len());
    }

    #[test]
    fn mapping_maps_all_tasks() {
        let tasks = mapping();
        let seq = sequence();

        assert_eq!(seq.len(), tasks.len());
        for s in seq {
            assert!(tasks.contains_key(&s));
        }
    }
}
