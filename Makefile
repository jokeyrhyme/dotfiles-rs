.PHONY: all
all: clean fmt test build

.PHONY: build
build:
	cargo build --all-features --all-targets --workspace

.PHONY: clean
clean:
	rm -rf target

.PHONY: fmt
fmt:
	cargo fmt

.PHONY: test
test:
	cargo fmt -- --check
	cargo clippy --all-features --all-targets --workspace
	cargo test --all-features --all-targets --workspace

